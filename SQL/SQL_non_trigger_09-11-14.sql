
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `ID` varchar(4) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Nombre` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES ('C1','Móviles'),('C2','Ordenadores');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `Dni` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Nombre` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Apellidos` varchar(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Telefono` int(11) NOT NULL,
  `Domicilio` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`Dni`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES ('24159454R','Antonio','Fernández Pérez',958111111,'Calle Palomino, 44'),('77140684H','Sergio','Martín Vílchez',669343210,'Calle Magnolia, 12');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `Cif` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Nombre` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Telefono` int(11) NOT NULL,
  `Direccion` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Nventas` int(4) NOT NULL,
  `VentasTotal` int(4) NOT NULL,
  PRIMARY KEY (`Cif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES ('77140684H','Mercadona SA',974515151,'Calle Bios,23, Granada',0,0),('A03039104','Consolas JaviGranada',964654456,'Calle de las Marionetas, 23, G',0,0),('A28017895','El Corte Inglés',974541241,'Calle Anselmo,32, Granada',0,0);
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `facturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturas` (
  `idventa` int(4) NOT NULL,
  `nfactura` int(4) NOT NULL,
  PRIMARY KEY (`idventa`),
  UNIQUE KEY `idventa` (`idventa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `facturas` WRITE;
/*!40000 ALTER TABLE `facturas` DISABLE KEYS */;
INSERT INTO `facturas` VALUES (2134,4),(3234,3);
/*!40000 ALTER TABLE `facturas` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `gastos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gastos` (
  `Luz` float NOT NULL,
  `Agua` float NOT NULL,
  `Internet` float NOT NULL,
  `Alquiler` float NOT NULL,
  `Autonomos` float NOT NULL,
  `seguro` float NOT NULL,
  `extra1` float NOT NULL,
  `extra2` float NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `gastos` WRITE;
/*!40000 ALTER TABLE `gastos` DISABLE KEYS */;
INSERT INTO `gastos` VALUES (12,123,34,456,45,45,4,4,'2014-01-01'),(25,35.99,12.95,120.33,300,0,30,0,'2014-09-01'),(25,52.33,30.99,300,300,150,11,0,'2014-10-01'),(123,123,1,1,1,1,1,1,'2014-11-04');
/*!40000 ALTER TABLE `gastos` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `gastostotales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gastostotales` (
  `gastos` double NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `gastostotales` WRITE;
/*!40000 ALTER TABLE `gastostotales` DISABLE KEYS */;
INSERT INTO `gastostotales` VALUES (400,'2014-10-01');
/*!40000 ALTER TABLE `gastostotales` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `ref` varchar(14) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(140) COLLATE utf8_spanish_ci NOT NULL,
  `pvp` double NOT NULL,
  `pEmpresa` double NOT NULL,
  `Pempresaplus` double NOT NULL,
  `stock` int(4) NOT NULL,
  `categoria` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `pvd` double NOT NULL,
  `fecha` date NOT NULL,
  `proveedor` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES ('101','Ordenador i5 Negroq','sdadsadasd',21312,1231,1231,0,'C2',123,'2014-10-04',''),('102','HTC ONE','asdadas',1232213,12312,123,10,'C1',123,'2014-10-04',''),('107','Movil HTC M9','Un movil de nueva generacion',14,15,13,48,'C1',45,'2014-10-07','ZonaInformatica Arabial'),('150','Movilazo','ewewq',12,12,2,0,'C1',1,'2014-10-07','Informática PepeCohete');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES (6,'Informática PepeCohete'),(3,'LG Electronics Granada'),(9,'MediaGaming Sur'),(4,'Pixelarium Studio Software'),(5,'ZonaInformatica Arabial');
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventas` (
  `id` int(4) NOT NULL,
  `cliente` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `total` double NOT NULL,
  `tipo` varchar(7) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `ventas` WRITE;
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
INSERT INTO `ventas` VALUES (0,'31152735f',100,'cliente','2014-07-16'),(1,'78984154F',100,'cliente','2014-07-16'),(2,'74879645F',56.04,'cliente','2014-07-16'),(3,'74568456F',578.55,'cliente','2014-07-16'),(5,'78945613B',100,'cliente','2014-07-16'),(6,'',100,'cliente','2014-07-16'),(7,'',0,'cliente','2014-07-16'),(8,'',100,'cliente','2014-07-16'),(9,'78987456F',0,'cliente','2014-07-16'),(10,'78987456F',100,'cliente','2014-07-16'),(11,'78456123D',100,'cliente','2014-07-16'),(12,'78468454F',0,'cliente','2014-07-21'),(13,'78468454F',100,'cliente','2014-07-21'),(14,'78987456F',100,'cliente','2014-07-21'),(15,'12345qwedF',0,'empresa','2014-07-21'),(16,'12345qwedF',100,'empresa','2014-07-21'),(17,'74775142E',100,'cliente','2014-07-21'),(18,'',100,'cliente','2014-07-27'),(19,'',22,'cliente','2014-08-26'),(20,'',38,'cliente','2014-08-26'),(21,'',12,'cliente','2014-08-26'),(22,'',12,'cliente','2014-08-26'),(23,'',28,'cliente','2014-08-29'),(24,'',38,'cliente','2014-08-29'),(25,'',12,'cliente','2014-08-29'),(26,'',12,'cliente','2014-08-29'),(27,'78465123D',12,'cliente','2014-08-29'),(28,'',12,'cliente','2014-08-29'),(29,'',12,'cliente','2014-08-29'),(30,'',11,'cliente','2014-08-29'),(31,'',102,'cliente','2014-08-29'),(32,'',101.65289256198348,'cliente','2014-09-02'),(33,'',101.65289256198348,'cliente','2014-09-02'),(34,'',101.65289256198348,'cliente','2014-09-02'),(35,'',101.65289256198348,'cliente','2014-09-02'),(36,'',0,'cliente','2014-09-02'),(37,'',101.65289256198348,'cliente','2014-09-02'),(38,'',101.65289256198348,'cliente','2014-09-02'),(39,'',101.65289256198348,'cliente','2014-09-02'),(40,'',203.30578512396696,'cliente','2014-09-02'),(41,'',208.26446280991738,'cliente','2014-09-02'),(42,'',304.9586776859504,'cliente','2014-09-02'),(43,'',3.3057851239669422,'cliente','2014-09-02'),(44,'',9.917355371900827,'cliente','2014-09-12'),(45,'',9.917355371900827,'cliente','2014-09-12'),(46,'77140684H',396.6942148760331,'cliente','2014-09-22'),(47,'77140684H',480,'cliente','2014-09-23'),(48,'77140684H',552.6,'cliente','2014-09-26');
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

