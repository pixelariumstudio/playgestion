/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package PlayGestion;

import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Sergio
 */
public class Exportar extends javax.swing.JFrame {
    private JFileChooser seleccion = new JFileChooser();
    private String usuario = "playporning";
    private String password = "themaster123";
    private String nombrebd = "tuningstore";
    private String comando = "C://xampp//mysql//bin//mysqldump -u"+usuario+" -p"+password+" --skip-comments --skip-triggers "+nombrebd;
    private int BUFFER = 10485760;
    
    Process proceso;
    ProcessBuilder constructorproceso;
    
    /**
     * Creates new form Exportar
     */
    public Exportar() {
        initComponents();
        setLocationRelativeTo(null);
        this.setResizable(false);
        setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/recursos/icono.jpg")));
    }

    /**
     * Nota para Sergio: Cuando hagas una ventana elimina el MAin, que eso puede tirar conflictos.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        rutaexportar = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        botonexportr = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Exportar base de datos MySQLDump");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/exportar.png"))); // NOI18N
        jLabel1.setText("Exportar base de datos.");

        jButton3.setText("Examinar...");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        botonexportr.setBackground(new java.awt.Color(0, 102, 255));
        botonexportr.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        botonexportr.setText("Exportar");
        botonexportr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonexportrActionPerformed(evt);
            }
        });

        jLabel2.setText("Insertar ruta manualmente o examinar ruta");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(rutaexportar, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonexportr, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rutaexportar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3)
                    .addComponent(botonexportr))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        if (seleccion.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
            rutaexportar.setText(seleccion.getSelectedFile().getAbsolutePath()+".sql");
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void botonexportrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonexportrActionPerformed
        try {
      Process p = Runtime
            .getRuntime()
            .exec(comando);
            
     File tst = new File(rutaexportar.getText());
 FileWriter fw = null;
 try {
 fw = new FileWriter(tst);
 fw.close();
 } catch (IOException ex) {
 ex.printStackTrace();
 }
 Runtime rt = Runtime.getRuntime();
 try {
 Process proc = rt.exec(comando);
 InputStream in = proc.getInputStream();
 InputStreamReader read = new InputStreamReader(in, "latin1");
 BufferedReader br = new BufferedReader(read);
 BufferedWriter bw = new BufferedWriter(new FileWriter(tst, true));
 String line = null;
 StringBuffer buffer = new StringBuffer();
 
int count;
 char[] cbuf = new char[BUFFER];
 while ((count = br.read(cbuf, 0, BUFFER)) != -1) {
 buffer.append(cbuf, 0, count);
 }
 String toWrite = buffer.toString();
 bw.write(toWrite);
 bw.close();
 br.close();
 JOptionPane.showMessageDialog(null, "Base de datos exportada correctamente.", "Exportación completada", JOptionPane.INFORMATION_MESSAGE);
 } catch (IOException e) {
 e.printStackTrace();
 }
        } catch (IOException ex) {
            ex.getMessage();
        }
      
    }//GEN-LAST:event_botonexportrActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonexportr;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField rutaexportar;
    // End of variables declaration//GEN-END:variables
}
