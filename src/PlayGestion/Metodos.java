/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PlayGestion;

import static PlayGestion.VentanaPrincipal.conexion;
import static PlayGestion.VentanaPrincipal.sentenciaSQL;
import static PlayGestion.VentanaPrincipal.textocon;
import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Pixelarium Studio
 */
public class Metodos {
    /*Crear un XML de configuraci�n
    
     */

    public static void crearconfig() {

        try {

            File xmlFile = new File("src/config/config.xml");
            org.jdom2.Element company = new org.jdom2.Element("config");
            org.jdom2.Document doc = new org.jdom2.Document(company);
            
            //doc.setRootElement(company);
            /*
            <config>
                <contrasenias>
                    <anularyborrar>pepe</anularyborrar>
                </contrasenias>
                
            </config>
            */
            org.jdom2.Element staff = new org.jdom2.Element("contrasenias");
            staff.setAttribute("id", "1");
            staff.addContent(new org.jdom2.Element("anularyborrar").setText(Configuracion.pass2.getText()));

            doc.getRootElement().addContent(staff);
            /*
             org.jdom2.Element staff2 = new org.jdom2.Element("staff");
             staff2.setAttribute("id", "2");
             staff2.addContent(new org.jdom2.Element("firstname").setText("low"));
             staff2.addContent(new org.jdom2.Element("lastname").setText("yin fong"));
             staff2.addContent(new org.jdom2.Element("nickname").setText("fong fong"));
             staff2.addContent(new org.jdom2.Element("salary").setText("188888"));
 
             doc.getRootElement().addContent(staff2);
             */
            // new XMLOutputter().output(doc, System.out);
            XMLOutputter xmlOutput = new XMLOutputter();

            // display nice nice
            xmlOutput.setFormat(Format.getPrettyFormat());
            xmlOutput.output(doc, new FileWriter("src/config/config.xml"));

            JOptionPane.showConfirmDialog(null, "Inicie el programa de nuevo para comenzar su uso.", null, JOptionPane.DEFAULT_OPTION);

            System.exit(0);

        } catch (IOException io) {
            System.out.println(io.getMessage());
        }

    }

    public static int contadorcategoria(ResultSet query) {
        ResultSet dos = query;
        int numero = 0;
        try {
            while (dos.next()) {
                numero++;
            }
        } catch (SQLException ex) {

        }
        return numero;
    }

    public static void conexion() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/tuningstore", "PlayGranada", "PlayGranada-PopCorn123");
            sentenciaSQL = conexion.createStatement();
            VentanaPrincipal.textocon.setText("�conectado!");
            VentanaPrincipal.textocon.setForeground(Color.green);
        } catch (SQLException er) {
            java.awt.Toolkit.getDefaultToolkit().beep();
            JOptionPane.showMessageDialog(null, er, "Error en la conexi�n", JOptionPane.ERROR_MESSAGE);
            textocon.setText("�no conectado!!");
            textocon.setForeground(Color.red);

        }

    }

    public static ResultSet obtenerproveedores() {
        ResultSet resultado = null;
        try {
            resultado = sentenciaSQL.executeQuery("SELECT nombre FROM proveedores");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }

    public static ResultSet selectclientes() {
        ResultSet resultado = null;
        try {
            resultado = sentenciaSQL.executeQuery("SELECT * FROM clientes");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }

    public static ResultSet referencia(String producto) {
        ResultSet resultado = null;
        try {
            resultado = sentenciaSQL.executeQuery("SELECT ref FROM productos where nombre = '" + producto + "'");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }

    public static ResultSet selectclientesord() {
        ResultSet resultado = null;
        try {
            resultado = sentenciaSQL.executeQuery("SELECT * FROM clientes order by 3 asc");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }

    public static ResultSet selectcategorias() {
        ResultSet resultado = null;
        try {
            resultado = sentenciaSQL.executeQuery("SELECT * FROM categorias");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }

    public static ResultSet selectcategoriasnombre() {
        ResultSet resultado = null;
        try {
            resultado = sentenciaSQL.executeQuery("SELECT Nombre FROM categorias");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }

    public static ResultSet selectempresa() {
        ResultSet resultado = null;
        try {
            resultado = sentenciaSQL.executeQuery("SELECT * FROM empresa");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }

    public static ResultSet selectdatoscliente(String dni) {
        ResultSet resultado = null;
        try {
            resultado = sentenciaSQL.executeQuery("SELECT * FROM clientes where dni ='" + dni + "'");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }

    public static void exportarbd() {
        JFrame exportar = new Exportar();
        exportar.setVisible(true);
    }

    public static ResultSet selectdatosempresa(String dni) {
        ResultSet resultado = null;
        try {
            resultado = sentenciaSQL.executeQuery("SELECT * FROM empresa where cif ='" + dni + "'");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }

    /**
     * Busca por Categoria,referencia,nombe o sin criterios en la tabla
     * productos Al ser necesaria la condicion evalua si es global o en su
     * defecto hace la busqueda por la condicion = a lo buscado
     *
     * @author Tom�s Hern�ndez
     * @param buscado aquello que buscamos
     * @param condicion la condicion por la que lo bsucaremos
     * @return resultset
     */
    public static ResultSet buscador(String buscado, String condicion) {
        //Creamos una cadena que esta preformateada, condicion ser� aquello por lo que busquemos y lo tendremos siempre a mano desde aqu�
        String tofind = String.format(" select * from productos where %s = '", condicion);
        ResultSet resultadico = null;
        if (condicion.equals("glob")) {
            try {
                resultadico = VentanaPrincipal.sentenciaSQL.executeQuery("select * from productos");
            } catch (SQLException ex) {
                Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                resultadico = VentanaPrincipal.sentenciaSQL.executeQuery(tofind + buscado + "'");
            } catch (SQLException ex) {
                Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return resultadico;
    }

    public static ResultSet selectclientedni(String dni) {
        ResultSet resultado = null;
        try {
            resultado = sentenciaSQL.executeQuery("SELECT nombre,apellidos FROM clientes where DNI ='" + dni + "'");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }

    public static ResultSet selectempresacif(String dni) {
        ResultSet resultado = null;
        try {
            resultado = sentenciaSQL.executeQuery("SELECT nombre FROM empresa where CIF ='" + dni + "'");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }

    public static DefaultListModel rellenarlista() {
        DefaultListModel modlist = new DefaultListModel();
        try {
            ResultSet res = sentenciaSQL.executeQuery("Select nombre, stock from productos where stock < 3");

            while (res.next()) {
                modlist.addElement("Producto: " + res.getString(1) + " --- Stock disponible: " + res.getString(2) + ".");

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return modlist;
    }

    public static String getMes(int themes) {
        String mes = "";
        themes--;

        switch (themes) {
            case 0:
                mes = "enero";
                break;
            case 1:
                mes = "febrero";
                break;
            case 2:
                mes = "marzo";
                break;
            case 3:
                mes = "abril";
                break;
            case 4:
                mes = "mayo";
                break;
            case 5:
                mes = "junio";
                break;
            case 6:
                mes = "julio";
                break;
            case 7:
                mes = "agosto";
                break;
            case 8:
                mes = "septiembre";
                break;
            case 9:
                mes = "octubre";
                break;
            case 10:
                mes = "noviembre";
                break;
            case 11:
                mes = "diciembre";
                break;
        }
        return mes;
    }

    public static String getDia(int thedia) {
        String dia = "";
        switch (thedia) {
            case 2:
                dia = "Lunes";
                break;
            case 3:
                dia = "Martes";
                break;
            case 4:
                dia = "Miercoles";
                break;
            case 5:
                dia = "Jueves";
                break;
            case 6:
                dia = "Viernes";
                break;
            case 7:
                dia = "S�bado";
                break;
            case 1:
                dia = "Domingo";
                break;
        }

        return dia;
    }

    /**
     * Metodo al cual le pasas un dia y un a�o y te devuelve los gastos de ese
     * mes en un ArrayList. Si la consulta no devuelve nada la posicion 0 del
     * arrayList sera false
     *
     * @author Tom�s Hern�ndez
     * @param mes int mes a buscar
     * @param year int a�o a buscar
     * @return Arraylist
     *
     */
    public static ArrayList getGastos(int mes, int year) {
        ArrayList resultado = new ArrayList();
        try {
            ResultSet res = sentenciaSQL.executeQuery("SELECT * FROM `gastos` WHERE month(fecha) =" + mes + " and year(fecha) =" + year);
            if (res.next()) {
                resultado.add(res.getString(1));
                resultado.add(res.getString(2));
                resultado.add(res.getString(3));
                resultado.add(res.getString(4));
                resultado.add(res.getString(5));
                resultado.add(res.getString(6));
                resultado.add(res.getString(7));
                resultado.add(res.getString(8));
            } else {
                resultado.add("false");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, ex);

        }
        return resultado;
    }

    /**
     * El metodo getGastoStock nos devuelve el importe total de todos los gastos
     * en un mes determinado en un String formateado a decimal
     *
     * @author Tomas Hernandez
     * @param mes int
     * @return gasto String formateado a 2 decimales
     */
    public static String getGastoStock(int mes) {
        String gasto = "0";
        DecimalFormat formato;
        formato = new DecimalFormat("###,###.##");
        try {
            ResultSet consultado = sentenciaSQL.executeQuery("select * FROM `gastostotales` where month(fecha)=" + mes);
            if (consultado.next()) {
                gasto = formato.format(Double.valueOf(consultado.getString(1)));
            }
        } catch (SQLException ex) {
            ex.getMessage();
        }
        return gasto;
    }
}
