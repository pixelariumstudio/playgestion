package PlayGestion;

import java.awt.Toolkit;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 *
 * @author Pixelarium Studio
 */
public class GastosMes extends javax.swing.JFrame {

    //variables de fecha
    private int mes;
    private int year;
    //formato
    DecimalFormat formato;

    public GastosMes(int mes, int year) {
        this.formato = new DecimalFormat("###,###.##");
        initComponents();
        setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/recursos/icono.jpg")));
        this.mes = mes;
        this.year = year;
        labelMes.setText(Metodos.getMes(mes));
        colocar();
    }

    private void colocar() {
        ArrayList gastos = Metodos.getGastos(mes, year);
        double gastosTotales = 0;
        String isFalse = (String) gastos.get(0);
        if (!isFalse.equals("false")) {
            labelLuz.setText((String) gastos.get(0));
            labelAgua.setText((String) gastos.get(1));
            labelInter.setText((String) gastos.get(2));
            labelAlquiler.setText((String) gastos.get(3));
            labelAuton.setText((String) gastos.get(4));
            labelSeguro.setText((String) gastos.get(5));
            float extras = Float.valueOf((String) gastos.get(6)) + Float.valueOf((String) gastos.get(7));//Sumamos las dos columnas de Extras
            String extrasFormat = formato.format(extras);
            labelExtra.setText(extrasFormat);
            //Label de Stock al encontrarse en otra tabla usamos un metodo para ello
            labelStock.setText(Metodos.getGastoStock(mes));
            double stock = Double.valueOf(Metodos.getGastoStock(mes));
            for (int i = 0; i < gastos.size(); i++) {
                gastosTotales = gastosTotales + Float.valueOf((String) gastos.get(i));
            }
            gastosTotales = gastosTotales + stock;
            String totalesFormat = formato.format(gastosTotales);
            textofinal.setText("El gasto total del mes asciende a [" + totalesFormat + "]");
        } else {
            labelLuz.setText("");
            labelAgua.setText("");
            labelInter.setText("");
            labelAlquiler.setText("");
            labelAuton.setText("");
            labelSeguro.setText("");
            labelStock.setText("");
            textofinal.setText("");
            labelExtra.setText("");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        labelLuz = new javax.swing.JLabel();
        labelInter = new javax.swing.JLabel();
        labelAgua = new javax.swing.JLabel();
        labelAuton = new javax.swing.JLabel();
        labelSeguro = new javax.swing.JLabel();
        labelExtra = new javax.swing.JLabel();
        textofinal = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        labelAlquiler = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        labelStock = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        labelMes = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        boxMeses = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Play-Gestion Gastos Mensuales");

        jPanel1.setBackground(new java.awt.Color(255, 255, 102));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Luz");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Agua");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Internet");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Autonomos");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Seguro");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Extras");

        labelLuz.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelLuz.setText("TotalL");

        labelInter.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelInter.setText("TotalL");

        labelAgua.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelAgua.setText("TotalL");

        labelAuton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelAuton.setText("TotalL");

        labelSeguro.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelSeguro.setText("TotalL");

        labelExtra.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelExtra.setText("TotalL");

        textofinal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        textofinal.setText("jLabel9");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setText("Alquiler");

        labelAlquiler.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelAlquiler.setText("TOTAL");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Compra de Stock:");

        labelStock.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelStock.setText("Total");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(textofinal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(142, 142, 142))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelLuz)
                            .addComponent(labelAgua)
                            .addComponent(labelInter)
                            .addComponent(labelAuton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 97, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel5)
                                .addComponent(jLabel6))
                            .addComponent(jLabel10)
                            .addComponent(jLabel9))
                        .addGap(36, 36, 36)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(labelExtra)
                                .addComponent(labelSeguro))
                            .addComponent(labelAlquiler)
                            .addComponent(labelStock))
                        .addGap(138, 138, 138))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelLuz)
                    .addComponent(jLabel10)
                    .addComponent(labelAlquiler))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(labelAgua)
                    .addComponent(jLabel5)
                    .addComponent(labelSeguro))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(labelInter)
                    .addComponent(jLabel6)
                    .addComponent(labelExtra))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(labelAuton)
                    .addComponent(jLabel9)
                    .addComponent(labelStock))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
                .addComponent(textofinal, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );

        jPanel2.setBackground(new java.awt.Color(51, 153, 255));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Mes:");

        labelMes.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelMes.setText("jLabel8");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Meses");

        boxMeses.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre","Diciembre" }));

        jButton1.setText("Ver");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(labelMes)
                .addGap(131, 131, 131)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(boxMeses, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 70, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(53, 53, 53))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(labelMes)
                    .addComponent(jLabel8)
                    .addComponent(boxMeses, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(16, 16, 16))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        mes = boxMeses.getSelectedIndex() + 1;
        labelMes.setText(Metodos.getMes(mes));
        colocar();
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox boxMeses;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel labelAgua;
    private javax.swing.JLabel labelAlquiler;
    private javax.swing.JLabel labelAuton;
    private javax.swing.JLabel labelExtra;
    private javax.swing.JLabel labelInter;
    private javax.swing.JLabel labelLuz;
    private javax.swing.JLabel labelMes;
    private javax.swing.JLabel labelSeguro;
    private javax.swing.JLabel labelStock;
    private javax.swing.JLabel textofinal;
    // End of variables declaration//GEN-END:variables
}
